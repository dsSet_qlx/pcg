<?php
// System Setup
require 'includes/startup.php';

// Variable Setup
$_POST  = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
$_GET  = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
// $db = new PDO('sqlite:login.sqlite3');
$user_agent = $_SERVER['HTTP_USER_AGENT'];
$user_ip = $_SERVER['REMOTE_ADDR'];
$user_time = time();

if (Config::SERVER != 'maintenance' OR (Config::SERVER == 'maintenance' AND $_GET['status'] == 'bsm')) { // Display site when not in maintenance mode or when bypassing maintenance lock with status

  if ($_GET['logout']) {

    new_activity_log($_SESSION['uid'], 'logged out');

    // Destroy session and return user to login screen
    session_destroy();
    header('Location: ' . Config::PROTOCOL . $_SERVER['SERVER_NAME'] . '/auth.php');
    die();

  } elseif (!$_POST) { // Display login form if no credentials posted

  if ($_GET['status'] == 'ready') { // Display appropriate page title for Google Analytics
    $title_status = 'TC Signup Successful';
  } elseif ($_GET['status'] == 'duplicate') {
    $title_status = 'TC Signup Unsuccessful Duplicate Email';
  } elseif ($_GET['status'] == 'reset') {
    $title_status = 'TC Password Reset Successful';
  } else {
    $title_status = 'TC Signin';
  }

  // Display HTML header
  echo $templates->render('layout-headout', [
    'page' => 'login',
    'title' => $title_status
  ]
);

  // Display login view
  echo $templates->render('view-login',
    [
      'page' => 'login',
      'email' => $_GET['email'],
      'status' => $_GET['status']
    ]);

  } else { // Check and verify credentials if posted

    $user_email = strtolower($_POST['user']);
    $pass = $_POST['pass'];

    // Connect to database
    try {
      $user = json_decode(json_encode(find_user($user_email)), true);
    } catch (Exception $e) {
      echo $e->getMessage();
      die();
    }

    $user_pass = $user[0]['password'];
    $user_id = $user[0]['_id']['$oid'];
    $user_firstName = $user[0]['firstName'];
    $user_lastName = $user[0]['lastName'];
    $user_email = $user[0]['email'];
    $user_avatar = $user[0]['avatar'];
    $educations = $user[0]['educations'];
    $conversation_views = $user[0]['conversationViews'];
    $notification_timestamp = $user[0]['notificationTimestamp']['$date'];
    $user_trusted = $user[0]['trusted'];
    $user_lastLogin = $user[0]['lastLogin']['$date']/1000;

    if (password_verify($pass, $user_pass)) { // Set session and redirect user if credentials verified

      // $user_outcome = 'success';
      // try {
      //   $db->exec("INSERT INTO attempts (user_time, user_ip, user_agent, user_email, user_outcome) VALUES ('$user_time', '$user_ip', '$user_agent', '$user_email', '$user_outcome');");
      // } catch (Exception $e) {
      //   echo $e->getMessage();
      //   die();
      // }

      session_regenerate_id();

      // Connect to database
      try {
        $groups = json_decode(json_encode(get_groups()), true);
        $user_groups = json_decode(json_encode(get_groups_by_user($user_id)), true);
      } catch (Exception $e) {
        echo $e->getMessage();
        die();
      }

      // Build list of affiliated partners
      $_SESSION['partners'] = [];
      $_SESSION['myGroups'] = [];
      $_SESSION['groups'] = [];
      foreach ($user_groups as $group) {
        $_SESSION['partners'][] = array(id => $group['_id']['$oid'], name => $group['name'], image => $group['tile']);
        $_SESSION['myGroups'][] = $group['_id']['$oid'];
      }
      foreach ($groups as $group) {
        $_SESSION['groups'][$group['_id']['$oid']] = $group['name'];
      }

      $_SESSION['uid'] = $user_id;
      $_SESSION['firstName'] = $user_firstName;
      $_SESSION['lastName'] = $user_lastName;
      $_SESSION['avatar'] = $user_avatar;
      $_SESSION['user'] = $user_ip;
      $_SESSION['email'] = $user_email;
      $_SESSION['conversations'] = $conversation_views;
      if ($user_trusted == TRUE) {
        $_SESSION['trusted'] = 'yes';
      } else {
        $_SESSION['trusted'] = 'no';
      }

      // Setup notifications status
      if ($notification_timestamp == NULL) {

        $new_timestamp = json_decode(json_encode(update_notifications_timestamp()), true);

        if ($new_timestamp != false) {

          $notification_timestamp = $new_timestamp['$date']['$numberLong'];

        } else {

          $notification_timestamp = "nothing";

        }
      }

      $_SESSION['notificationTimestamp'] = $notification_timestamp;

      update_last_login_timestamp();
      update_ip_address();
      update_total_logins();

      $activity_data[] = $user_ip;
      $activity_data[] = $user_agent;
      $activity_data[] = $user_email;
      new_activity_log($_SESSION['uid'], 'logged in', $activity_data);

      if ($_GET['status'] == 'bsm') { // Bypass server maintenance
        $_SESSION['bsm'] = 1;
      }

      // If first time visitor or haven't logged in since April 1 2019, redirect to guided tour
      if (!$user_lastLogin OR $user_lastLogin < 1554076800) {
        require_once 'includes/detect-mobile.php';
        $detect = new Mobile_Detect;
        if ($detect->isMobile()) { } else {
          header('Location: ' . Config::PROTOCOL . $_SERVER['SERVER_NAME'] . '/tour/topics.php');
          die();
        }
      }

      if ($_GET['location']) {
        header('Location: ' . Config::PROTOCOL . $_SERVER['SERVER_NAME'] . $_GET['location']);
      } else {
        header('Location: ' . Config::PROTOCOL . $_SERVER['SERVER_NAME'] . '/home.php');
      }
      die();

    } else { // Display error message and login form if credentials don't verify

      // $user_outcome = 'fail';
      // try {
      //   $db->exec("INSERT INTO attempts (user_time, user_ip, user_agent, user_email, user_outcome) VALUES ('$user_time', '$user_ip', '$user_agent', '$user_email', '$user_outcome');");
      // } catch (Exception $e) {
      //   echo $e->getMessage();
      //   die();
      // }

      $activity_data[] = $user_ip;
      $activity_data[] = $user_agent;
      $activity_data[] = $user_email;
      new_activity_log(0, 'failed login', $activity_data);

      // Display HTML header
      echo $templates->render('layout-headout', [
        'page' => 'login',
        'title' => 'TC Signin Unsuccessful',
        'block' => 'login-fail'
      ]
    );

      // Display login view
      echo $templates->render('view-login', ['page' => 'login-fail']);

    }

  }

} else {

  session_destroy();
  header('Location: ' . Config::PROTOCOL . $_SERVER['SERVER_NAME'] . '/maintenance.php');
  die();

}
