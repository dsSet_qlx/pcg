<?php

// format email for reset passwords
function formatEmailReset($reset_start, $user_firstName) {
	$email_info = [];
	$email_info['subject'] = "Reset your password at TeachersConnect";
	$email_info['body'] = "<p>Hi " . $user_firstName . ", <br><br>We recently received a request to reset your account password at TeachersConnect. In order to complete this request, you will need to provide us with a new password.<br><br>Please click on this link to set your new password:<br><br><a style='background:#E56841;color:#FFFFFF;text-decoration:none;font-weight:bold;font-size:1.5em;padding:5px 10px;border-radius:4px;' href='" . Config::PROTOCOL . $_SERVER['SERVER_NAME'] . '/reset.php?id=' . $reset_start . "'>Reset Password</a><br><br><i>Please note: This link can only be used once and will become inactive in one hour.  If you do not need to reset your password, please ignore this email.</i><br><br>
</p>";
	$email_info['header'] = "<div style='background: #F9CE28; border-bottom: 1px #D4AA07 solid; text-align: center;'><a href='" . Config::PROTOCOL . $_SERVER['SERVER_NAME'] . "/'><img style='width: 265px; padding: 10px;' alt='TeachersConnect' src='cid:header-logo'></a></div>";
	return $email_info;
}

// format email notifications -katie
function formatEmail($notification_type, $first_name, $responder_name, $link, $notification_settings, $initial_name, $preview_content) {
	$email_info = [];
  foreach ($notification_settings as $notification_name => $notification_setting) {
    if ( ( $notification_type == $notification_name ) AND ( $notification_setting == 1 ) ) {
      if ($notification_type == "answer") {
        $email_info['subject'] = "A question has been answered on TeachersConnect";
        $email_info['body'] = "<p>Hi " . $first_name . ", <br><br>";
				if ($initial_name == "anonymous") {
					$email_info['body'] = $email_info['body'] . "Someone";
				} else {
					$email_info['body'] = $email_info['body'] . ucwords(strtolower($responder_name));
				}
				$email_info['body'] = $email_info['body'] . " wrote an answer to the following question on TeachersConnect:<br><b>\"" . $preview_content . "\"</b><br><br><a href='" . $link . "'>" . $link . "</a><br><br></p>";
      }
      else if ($notification_type == "comment") {
        $email_info['subject'] = "A post has a new comment on TeachersConnect";
				$email_info['body'] = "<p>Hi " . $first_name . ", <br><br>" . ucwords(strtolower($responder_name)) . " commented on the following post on TeachersConnect. Go take a look! <br>\"<b>" . $preview_content . "\"</b><br><br><a href='" . $link . "'>" . $link . "</a><br><br></p>";
      }
      else if ($notification_type == "follow") {
        $email_info['subject'] = "You have a new follower on TeachersConnect";
				$email_info['body'] = "<p>Hi " . $first_name . ", <br><br>" . ucwords(strtolower($responder_name)) . " started following you on TeachersConnect. Go take a look! <br><br><a href='" . $link . "'>" . $link . "</a><br><br></p>";
      }
			// $path = Config::PROTOCOL . $_SERVER['SERVER_NAME'] . '/img/tclogo.png';
			// $type = pathinfo($path, PATHINFO_EXTENSION);
			// $imagedata = file_get_contents($path);
			// $base64 = 'data:image/' . $type . ';base64,' . base64_encode($imagedata);
			$email_info['header'] = "<div style='background: #F9CE28; border-bottom: 1px #D4AA07 solid; text-align: center;'><a href='" . Config::PROTOCOL . $_SERVER['SERVER_NAME'] . "/'><img style='width: 265px; padding: 10px;' alt='TeachersConnect' src='cid:header-logo'></a></div>";
			$email_info['footer'] = "<p style='font-size: 10px;'><a href='" . Config::PROTOCOL . $_SERVER['SERVER_NAME'] . "/edit-notifications.php'>Click here to update your email notification preferences</a></p>";
			$email_info['footer-plain'] = "\r\nUpdate your email notification preferences here: " . Config::PROTOCOL . $_SERVER['SERVER_NAME'] . "/edit-notifications.php";
    }
  }
  return $email_info;
}

//send notification email -katie
function sendEmail ($full_name, $email_address, $email_format) {

  $email = new \SendGrid\Mail\Mail();
	$email->addCategory("notification");
  $email->setFrom("hello@teachersconnect.com", "TeachersConnect");
  $email->setSubject($email_format['subject']);
  $email->addTo($email_address, $full_name);
	$email->addContent("text/plain", strip_tags($email_format['body']) . $email_format['footer-plain']);
	$email->addContent("text/html", $email_format['header'] . $email_format['body'] . $email_format['footer']);
	$file_encoded = base64_encode(file_get_contents(Config::PROTOCOL . $_SERVER['SERVER_NAME'] . '/img/tclogo.png'));
	$email->addAttachment(
	    $file_encoded,
	    "application/png",
	    "tclogo.png",
	    "inline",
			"header-logo"
	);
	//trying sendgrid templates
	//$email->setTemplateId("d-f5770013045c47dea29d521233a1ad7f");
  $sendgrid = new \SendGrid(Config::SENDGRID_KEY);

    try {
        $response = $sendgrid->send($email);
        //print $response->statusCode() . "\n";
        //print_r($response->headers());
        //print $response->body() . "\n";
    } catch (Exception $e) {
        //echo 'Caught exception: ',  $e->getMessage(), "\n";
        echo 'Something went wrong';
        die();
    }
  }


//send notification email -katie
function sendEmailTemplate ($full_name, $email_address, $email_format, $template_id) {

  $email = new \SendGrid\Mail\Mail();
  $email->setFrom("hello@teachersconnect.com", "TeachersConnect");
  $email->setSubject($email_format['subject']);
  $email->addTo($email_address, $full_name);
	$email->addContent("text/plain", strip_tags($email_format['body']) . $email_format['footer-plain']);
	$email->addContent("text/html", $email_format['header'] . $email_format['body'] . $email_format['footer']);
	//changing path to logo png
	$file_encoded = base64_encode(file_get_contents('C:\xampp\htdocs\dev-staging\img\tclogo.png'));
	//$file_encoded = base64_encode(file_get_contents(Config::PROTOCOL . $_SERVER['SERVER_NAME'] . '/img/tclogo.png'));
	$email->addAttachment(
	    $file_encoded,
	    "application/png",
	    "tclogo.png",
	    "inline",
			"header-logo"
	);
	//trying sendgrid templates
	$email->setTemplateId("d-f5770013045c47dea29d521233a1ad7f");
  $sendgrid = new \SendGrid(Config::SENDGRID_KEY);

    try {
        $response = $sendgrid->send($email);
        //print $response->statusCode() . "\n";
        //print_r($response->headers());
        //print $response->body() . "\n";
    } catch (Exception $e) {
        //echo 'Caught exception: ',  $e->getMessage(), "\n";
        echo 'Something went wrong';
        die();
    }
  }
