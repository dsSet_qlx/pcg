<?php
// System Setup
session_start();
require 'includes/config.php';
require APP_VENDOR_INCLUDE_PATH;
require 'includes/database.php';
require 'includes/plates.php';
require 'includes/checkup.php';

if ($_SESSION['user'] == $_SERVER['REMOTE_ADDR']) { // Display view if user has valid session

  // // Determine courtesy status
  // $courtesy = 0;
  //
  // try {
  //   $affiliates_subscribed = json_decode(json_encode(get_groups_by_user($_SESSION['uid'])), true);
  // } catch (Exception $e) {
  //   echo $e->getMessage();
  //   die();
  // }
  //
  // foreach ($affiliates_subscribed as $group) {
  //   if ($group['paid'] == 1) {
  //     $courtesy = 1;
  //   }
  // }
  //
  // if ($_SESSION['freechoice'] == 1) {
  //   $courtesy = 1;
  // }

  // Provide all users with access to free membership
  $courtesy = 1;

?>

<html>
<head>
  <title>TeachersConnect Payment Options</title>
  <meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no" />
  <meta name="apple-mobile-web-app-title" content="TeachersConnect">
  <link rel="apple-touch-icon" href="img/icon-tc-app.png">
  <link rel="manifest" href="manifest.json">
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="css/normalize.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700|Patrick+Hand" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="css/styles.css?04042018">
  <?php if (Config::SERVER == 'production') { ?>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-69936049-13"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', 'UA-69936049-13');
  </script>
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-53H3NSS');</script>
  <?php } ?>
</head>
<body id="payment" class="pay">
<div id="">
  <div id="nav-bar" class="nav-bar row">
    <div class="header-block header-logo">
      <a href="home.php"><img class="logo" alt="TeachersConnect" src="img/tclogo-small.png"></a>
    </div>
    <div class="header-block right top-bar-button-group">
      <a class="top-bar-button" href="profile.php">
        <img class="top-bar-profile" src="img/icon-menu-profile.png">
      </a>
    </div>
  </div>
</div>
<div id="tour-navigation">
  <div class="tour-button col25">
  </div>
  <div class="tour-description col50">
    <h3>You matter.</h3>
    <div class="text">
      Teachers are the single most important factor in students’ academic success. Join a diverse family of kindred teaching spirits who lift you up, challenge you, and keep you growing.
      <br><br>
      Choose your plan today: Join for free or make a payment. Payments ensure that TeachersConnect is accessible to pre-service teachers, paraprofessionals, and teachers working multiple jobs.
    </div>
    <h4>Join today. Reach one more student tomorrow.</h4>
    <!-- <h3>You matter.<br>You're worth it.</h3>
    <div class="text">
      You’re the single most important factor in your students’ academic success. Join a diverse family of kindred teaching spirits who lift you up, challenge you, and keep you growing.
      <br><br>
      Choose your plan today, and reach one more student tomorrow.
    </div> -->
  </div>
  <div class="tour-button col25">
    &nbsp;
  </div>
</div>
<div id="tour-footer">
  <div class="col25">
    &nbsp;
  </div>
  <div class="col50">
    <?php if ($courtesy == 1) { ?>
      <a href="#" id="checkout-button-sku_courtesy" role="link"><img class="membership-courtesy" src="img/payment-option-4.png"></a>
      <script>
      (function() {
        var checkoutButton = document.getElementById('checkout-button-sku_courtesy');
        checkoutButton.addEventListener('click', function () {
          window.location = "<?=Config::PROTOCOL . $_SERVER['SERVER_NAME']?>/payment-process.php?status=courtesy";
        });
      })();
      </script>
    <?php } ?>
  </div>
  <div class="col25">
    &nbsp;
  </div>
</div>
<div id="tour-options">
  <a href="#" id="checkout-button-plan_GNYmR2iHCgPhrT" role="link"><img src="img/payment-option-1.png"></a>
  <a href="#" id="checkout-button-plan_GNYmWve5khT1Gq" role="link"><img src="img/payment-option-2.png"></a>
  <a href="#" id="checkout-button-sku_GNYfyMxpAH6Djy" role="link"><img src="img/payment-option-3.png"></a><br>
  <div id="error-message"></div>
</div>
<div id="tour-footer">
</div>
<!-- Load Stripe.js on your website. -->
<script src="https://js.stripe.com/v3"></script>
<script>
(function() {
  var stripe = Stripe('pk_live_o7iEWu1YyVgkOoNwrTKVI7uB00ggMWXh9W');
  var checkoutButton = document.getElementById('checkout-button-plan_GNYmR2iHCgPhrT');
  checkoutButton.addEventListener('click', function () {
    stripe.redirectToCheckout({
      items: [{plan: 'plan_GNYmR2iHCgPhrT', quantity: 1}],
      successUrl: '<?=Config::PROTOCOL . $_SERVER['SERVER_NAME']?>/payment-process.php?id={CHECKOUT_SESSION_ID}',
      cancelUrl: '<?=Config::PROTOCOL . $_SERVER['SERVER_NAME']?>/payment-options.php',
    })
    .then(function (result) {
      if (result.error) {
        var displayError = document.getElementById('error-message');
        displayError.textContent = result.error.message;
      }
    });
  });
})();
(function() {
  var stripe = Stripe('pk_live_o7iEWu1YyVgkOoNwrTKVI7uB00ggMWXh9W');
  var checkoutButton = document.getElementById('checkout-button-plan_GNYmWve5khT1Gq');
  checkoutButton.addEventListener('click', function () {
    stripe.redirectToCheckout({
      items: [{plan: 'plan_GNYmWve5khT1Gq', quantity: 1}],
      successUrl: '<?=Config::PROTOCOL . $_SERVER['SERVER_NAME']?>/payment-process.php?id={CHECKOUT_SESSION_ID}',
      cancelUrl: '<?=Config::PROTOCOL . $_SERVER['SERVER_NAME']?>/payment-options.php',
    })
    .then(function (result) {
      if (result.error) {
        var displayError = document.getElementById('error-message');
        displayError.textContent = result.error.message;
      }
    });
  });
})();
(function() {
  var stripe = Stripe('pk_live_o7iEWu1YyVgkOoNwrTKVI7uB00ggMWXh9W');
  var checkoutButton = document.getElementById('checkout-button-sku_GNYfyMxpAH6Djy');
  checkoutButton.addEventListener('click', function () {
    stripe.redirectToCheckout({
      items: [{sku: 'sku_GNYfyMxpAH6Djy', quantity: 1}],
      successUrl: '<?=Config::PROTOCOL . $_SERVER['SERVER_NAME']?>/payment-process.php?id={CHECKOUT_SESSION_ID}',
      cancelUrl: '<?=Config::PROTOCOL . $_SERVER['SERVER_NAME']?>/payment-options.php',
    })
    .then(function (result) {
      if (result.error) {
        var displayError = document.getElementById('error-message');
        displayError.textContent = result.error.message;
      }
    });
  });
})();
</script>

<div id="site-footer" class="site-footer col100">
  <ul class="footer-menu">
    <li><a target="_blank" rel="noopener" href="https://www.teachersconnect.com/terms-of-use/">Terms of Use</a></li>
    <li><a target="_blank" rel="noopener" href="https://www.teachersconnect.com/privacy-policy/">Privacy Policy</a></li>
    <li><a target="_blank" rel="noopener" href="https://www.teachersconnect.com/2017/08/10/community-guidelines/">Community Guidelines</a></li>
    <li><a target="_blank" rel="noopener" href="https://www.teachersconnect.com/support/">Support</a></li>
    <li><a rel="noopener" href="<?=Config::PROTOCOL . $_SERVER['SERVER_NAME']?>/auth.php?logout=1">Logout</a></li>
  </ul>
  <div class="footer-note">
    © Great Teachers Inc <?php echo date("Y"); ?> - All rights reserved
  </div>
</div>

</body>
</html>

<?php

} else { // Redirect user to login page if no valid session

  header('Location: ' . Config::PROTOCOL . $_SERVER['SERVER_NAME'] . '/auth.php?location=' . urlencode($_SERVER['REQUEST_URI']));

}

?>
