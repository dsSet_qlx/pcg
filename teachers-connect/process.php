<?php
// System Setup
require 'includes/startup.php';
require 'includes/checkup.php';
require 'includes/email.php';

if ($_SESSION['user'] == $_SERVER['REMOTE_ADDR']) { // Only process if user has valid session

  // Variable Setup
  $_GET  = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

  if (($_GET['type'] == "conversation") AND $_GET['id']) { // Delete conversation if ID supplied

    try {

      $conversation = json_decode(json_encode(get_conversation_by_id($_GET['id'])), true);

      if ($conversation) {

        if ($conversation[0]['owner'] == $_SESSION['uid']) {

          $conversationAction = json_decode(json_encode(delete_conversation($_GET['id'])), true);

          $activity_data[] = $_GET['id'];
          $activity_data[] = $conversation;
          new_activity_log($_SESSION['uid'], 'deleted conversation', $activity_data);

          header('Location: ' . Config::PROTOCOL . $_SERVER['SERVER_NAME'] . '/messages-inbox.php?alert=success-conversation-deleted');

          die();

        } else {

          $conversationAction = json_decode(json_encode(leave_conversation($_GET['id'])), true);

          $activity_data[] = $_GET['id'];
          new_activity_log($_SESSION['uid'], 'left conversation', $activity_data);

          header('Location: ' . Config::PROTOCOL . $_SERVER['SERVER_NAME'] . '/messages-inbox.php?alert=success-conversation-left');

          die();

        }

      }

    } catch (Exception $e) {
      echo $e->getMessage();
      die();
    }

  } elseif (($_GET['type'] == "post") AND $_GET['id']) { // Delete post if ID supplied

    try {
      $post = json_decode(json_encode(get_post($_GET['id'])), true);

      if ($post[0]['audience'] != NULL) {

        $feed_number = $post[0]['audience'];

      }

      if ($post) { // Collate and pull authors from database

        $author_id = $post[0]['userId'];

        if ($_SESSION['uid'] == $author_id) {

          $post_delete = delete_post($_GET['id']);

          if ($post_delete == false){

            echo 'Something went wrong';
            die();

          } else {

            $activity_data[] = $_GET['id'];
            $activity_data[] = $post;
            $activity_data[] = $post[0]['text'];
            new_activity_log($_SESSION['uid'], 'deleted post', $activity_data);

            if ($feed_number) {

              header('Location: ' . Config::PROTOCOL . $_SERVER['SERVER_NAME'] . '/feed.php?id=' . $feed_number . '&alert=success-post-deleted');

            } else {

              header('Location: ' . Config::PROTOCOL . $_SERVER['SERVER_NAME'] . '/feed.php?alert=success-post-deleted');

            }

          }

        }

      }
    } catch (Exception $e) {
      echo $e->getMessage();
      die();
    }

  } elseif (($_GET['type'] == "comment") AND $_GET['id']) { // Delete comment if ID supplied

    try {

      $post = json_decode(json_encode(get_comment($_GET['id'])), true);

      if ($post) {

        $post_id = $post[0]['_id']['$oid'];

        foreach ($post[0]['comments'] as $comment) {

          if ( ($comment['_id']['$oid'] == $_GET['id']) AND ($comment['userId'] == $_SESSION['uid']) ) {

            $comment_delete = delete_comment($post_id, $_GET['id']);

            if ($comment_delete == false){

              echo 'Something went wrong';
              die();

            } else {

              $activity_data[] = $post_id;
              $activity_data[] = $post;
              $activity_data[] = $comment['text'];
              new_activity_log($_SESSION['uid'], 'deleted comment', $activity_data);

              header('Location: ' . Config::PROTOCOL . $_SERVER['SERVER_NAME'] . '/view.php?id=' . $post_id . '&alert=success-comment-deleted');

            }

          }

        }

      }

    } catch (Exception $e) {
      echo $e->getMessage();
      die();
    }

  } elseif (($_GET['type'] == "education") AND $_GET['id']) { // Delete education history if ID supplied

    try {

          // Find and parse user education records
          $user = json_decode(json_encode(get_user($_SESSION['uid'])), true);

          foreach ($user[0]['educations'] as $education) {
            if ($education['_id']['$oid'] == $_GET['id']) {
              $name = $education['institude'];
            }
          }

          // Find group id and users by name
          $user_group = json_decode(json_encode(find_group($name)), true);

          if ($user_group) { // If user in group, remove on delete

            $gid = $user_group[0]['_id']['$oid'];
            $user_array = $user_group[0]['users'];

            if ($user_array == NULL) {
              $user_array = [];
            }

            if (in_array($_SESSION['uid'], $user_array)) { // Remove user from group array
              $uid[] = $_SESSION['uid'];
              $user_group = json_decode(json_encode(remove_group_user($gid, $uid)), true);
            }

            // Make sure user_array is an actual array
            if ($user_array == NULL) {
              $user_array = [];
            }

            if (is_array($user_array)) { } else {
              $user_array = array($user_array);
            }

            // Delete user from group
            $user_group_delete = update_group($gid, $user_array);

            $groups = json_decode(json_encode(get_groups_by_user($_SESSION['uid'])), true);

            // Build list of affiliated partners
            $_SESSION['partners'] = [];
            $_SESSION['myGroups'] = [];
            foreach ($groups as $group) {
                $_SESSION['partners'][] = array(id => $group['_id']['$oid'], name => $group['name'], image => $group['tile']);
                $_SESSION['myGroups'][] = $group['_id']['$oid'];
            }


          }

          // Delete education from user record
          $user_education_delete = delete_user_education($_SESSION['uid'], $_GET['id']);

          if ($user_education_delete == false){

            echo 'Something went wrong';
            die();

          } else {

            header('Location: ' . Config::PROTOCOL . $_SERVER['SERVER_NAME'] . '/edit-profile.php?alert=removed');

          }

    } catch (Exception $e) {
      echo $e->getMessage();
      die();
    }

  } elseif (($_GET['type'] == "experience") AND $_GET['id']) { // Delete experience history if ID supplied

          $user_experience_delete = delete_user_experience($_SESSION['uid'], $_GET['id']);

          if ($user_experience_delete == false){

            echo 'Something went wrong';
            die();

          } else {

            header('Location: ' . Config::PROTOCOL . $_SERVER['SERVER_NAME'] . '/edit-profile.php?alert=removed');

          }


  } elseif (($_GET['type'] == "affiliate") AND $_GET['id']) { // Unsubscribe from affiliate if ID supplied

    try {

          // Find group id and users by name
          // $user_group = json_decode(json_encode(find_group($_GET['id'])), true);
          $user_group = json_decode(json_encode(get_group($_GET['id'])), true);

          if ($user_group) { // Remove user if in group

            $gid = $user_group[0]['_id']['$oid'];
            $user_array = $user_group[0]['users'];

            if ($user_array == NULL) {
              $user_array = [];
            }

            if (in_array($_SESSION['uid'], $user_array)) { // Remove user from group array
              $uid[] = $_SESSION['uid'];
              $user_group = json_decode(json_encode(remove_group_user($gid, $uid)), true);
            }

            $groups = json_decode(json_encode(get_groups_by_user($_SESSION['uid'])), true);

            // Build list of affiliated partners
            $_SESSION['partners'] = [];
            $_SESSION['myGroups'] = [];
            foreach ($groups as $group) {
                $_SESSION['partners'][] = array(id => $group['_id']['$oid'], name => $group['name'], image => $group['tile']);
                $_SESSION['myGroups'][] = $group['_id']['$oid'];
            }

            $activity_data[] = $_GET['id'];
            new_activity_log($_SESSION['uid'], 'removed community', $activity_data);

          }

          header('Location: ' . Config::PROTOCOL . $_SERVER['SERVER_NAME'] . '/edit-profile.php?alert=removed');

    } catch (Exception $e) {
      echo $e->getMessage();
      die();
    }

  } elseif (($_GET['type'] == "follow") AND $_GET['id']) { // Follow user

    $responder_name = ucfirst($_SESSION['firstName']) . ' ' . ucfirst($_SESSION['lastName']);
    $link = Config::PROTOCOL . $_SERVER['SERVER_NAME'] . '/profile.php?id=' . $_SESSION['uid'];
    $notification_type = 'follow';
    $notify_users[] = $_GET['id'];

    try {

          // Follow new user
          $user_following_add = add_user_following($_GET['id']);

          // Create new notification
          $notification_new = new_notification($responder_name, $_SESSION['uid'], $_SESSION['uid'], $_SESSION['avatar'], $notification_type, '', $_GET['id'], $_SESSION['uid'], '', '', $notify_users);

          // Check email notification settings and send emails where appropriate
          foreach ($notify_users as $uid) {

            if ($uid != $_SESSION['uid']) {

              // Connect to database
              try {
                $user = json_decode(json_encode(get_user($uid)), true);
              } catch (Exception $e) {
                echo $e->getMessage();
                die();
              }

              $email_format = formatEmail($notification_type, $user[0]['firstName'], $responder_name, $link, $user[0]['emailNotifications']);

              if($email_format) {

                $email_sent = sendEmail($user[0]['firstName'] . " " . $user[0]['lastName'], $user[0]['email'], $email_format);

              }

            }

          }

          $activity_data[] = $_GET['id'];
          new_activity_log($_SESSION['uid'], 'followed member', $activity_data);

          header('Location: ' . Config::PROTOCOL . $_SERVER['SERVER_NAME'] . '/profile.php?id=' . $_GET['id'] . '&action=follow&alert=success');

    } catch (Exception $e) {
      echo $e->getMessage();
      die();
    }

  } elseif (($_GET['type'] == "unfollow") AND $_GET['id']) { // Unfollow user

    $uid[] = $_GET['id'];

    try {

          $user_following_remove = remove_user_following($uid);

          $activity_data[] = $_GET['id'];
          new_activity_log($_SESSION['uid'], 'unfollowed member', $activity_data);

          header('Location: ' . Config::PROTOCOL . $_SERVER['SERVER_NAME'] . '/profile.php?id=' . $_GET['id'] . '&action=unfollow&alert=success');

    } catch (Exception $e) {
      echo $e->getMessage();
      die();
    }

  } else {

    echo 'something missing';
    die();

  }

} else { // Redirect user to login page if no valid session

  header('Location: ' . Config::PROTOCOL . $_SERVER['SERVER_NAME'] . '/auth.php?location=' . urlencode($_SERVER['REQUEST_URI']));

}
