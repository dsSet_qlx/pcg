<?php if ($posts) { ?>
<div class="home-row hide">
<h2 class="carousel-title">
  <div class="show-feed">
    <?php if ($row_url == 'topics.php') { ?>
      <a href="topics.php">View all topics</a>
    <?php } elseif ($row_url) { ?>
      <a href="<?=$row_url?>">View all posts</a>
    <?php } ?>
  </div>
  <?php if ($row_url) { ?>
    <a href="<?=$row_url?>">
  <?php } ?>
  <?=$row_title?>
  <?php if ($row_url) { ?>
    </a> <span class="context-text">Recent Posts</span>
  <?php } ?>
</h2>
<div class="carousel" data-slick='{"slidesToShow": 4, "slidesToScroll": 4}'>
  <?php foreach ($posts as $post) { ?>

    <a href="view.php?id=<?=$post['id']?>">
    <div class="card">
      <div class="content">
        <div class="author">
          <div class="post-header col-avatar small">
            <?php if ($post['anon'] == 1) { ?>
              <img class="avatar" src="img/anon.svg">
            <?php } elseif ( (strpos($posts_authors[$post['author']]['avatar'], 'Object') == false) AND ($posts_authors[$post['author']]['avatar'] != NULL) ) { ?>
              <img class="avatar" src="" data-lazy="image.php?id=<?=$posts_authors[$post['author']]['avatar']?>&height=200" src="">
            <?php } else { ?>
              <img class="avatar" src="img/robot.svg">
            <?php } ?>
     	    </div>
          <div class="post-header">
            <div class="author-name">
              <?php if ($post['anon'] == 1) {
                echo "Anonymous";
              } else {
                echo $posts_authors[$post['author']]['firstName'] . " " . $posts_authors[$post['author']]['lastName'];
              } ?>
            </div>
            <div class="post-time" data-id="<?=$post['date']?>">
              <?=timestamp($post['date']);?>
            </div>
          </div>
        </div>
        <?php if ($post['featuredphoto']) { ?>
          <div class="img-preview img-featured img1of1">
            <img src="" data-lazy="image.php?id=<?=$post['featuredphoto']?>&height=400">
          </div>
        <?php } elseif (!$post['featuredphoto'] AND $post['youtube']) { ?>
          <div class="img-preview img-featured img1of1">
            <img src="" data-lazy="https://i.ytimg.com/vi/<?=$post['youtube']?>/hqdefault.jpg">
          </div>
        <?php } ?>
        <div class="preview-content">
          <?=$post['text']?>
        </div>
      </div>
      <div class="bottom-bar">

        <div class="post-reactions">
          <?php if ($post['views'] > 0) { ?>
            <!-- <img class="icon-react" src="img/icon-views.svg"><div class="reaction-count"><?=$post['views']?></div> -->
          <?php } ?>
          <?php if ($post['comments'] > 0) { ?>
            <img class="icon-react" src="img/icon-comments.svg"><div class="reaction-count"><?=$post['comments']?></div>
          <?php } ?>
          <?php if ($post['sameheres'] > 0) { ?>
            <img class="icon-react" src="img/icon-sameheres.svg"><div class="reaction-count"><?=$post['sameheres']?></div>
          <?php } ?>
          <?php if ($post['helpfuls'] > 0) { ?>
            <img class="icon-react" src="img/icon-helpfuls.svg"><div class="reaction-count"><?=$post['helpfuls']?></div>
          <?php } ?>
          <?php if ($post['files'] > 0) { ?>
            <img class="icon-react" src="img/icon-files.svg"><div class="reaction-count"><?=$post['files']?></div>
          <?php } ?>
        </div>
      </div>
    </div>
    </a>

<?php } ?>
</div>
<div class="feed-separator"></div>
</div>
<?php } ?>
