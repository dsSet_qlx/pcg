
    <div class="login-callout">
      <?php if ($status == 'duplicate') { // Display duplicate email message ?>
        <img class="login-callout-img-alert" src="img/icon-alert-red.svg">
        <div class="login-callout-alert-text">
          <h2 class="login-callout-alert red">An account with that email already exists.</h2>
          The email you provided while signing up already has an account on TeachersConnect. Please try logging in with your existing account password. If you continue to experience issues, please <a target="_blank" href="http://www.teachersconnect.com/support-request/">contact us here</a>.
        </div>
      <?php } elseif ($status == 'ready') { // Display account ready message ?>
        <img class="login-callout-img-alert" src="img/icon-alert-green.svg">
        <div class="login-callout-alert-text">
          <h2 class="login-callout-alert green">You’ve successfully created your account.</h2>
          You are ready to join the community. Please sign in to confirm your account.
        </div>
      <?php } elseif ($status == 'reset') { // Display password reset message ?>
        <img class="login-callout-img-alert" src="img/icon-alert-green.svg">
        <div class="login-callout-alert-text">
          <h2 class="login-callout-alert green">You’ve successfully reset your password.</h2>
          Welcome back to the community. Please sign in to confirm your new password is working correctly.
        </div>
      <?php } else { // Display welcome screen ?>
        <img class="login-callout-img-logo" src="img/tclogo2.svg">
        <img class="login-callout-img-community" src="img/tccommunity.png">
        <div class="login-callout-text">
          Uncompromisingly Teacher-centric. Moderated and trustworthy. No ads. No anonymous trolls. Only teachers solving classroom problems. Ask a question, share a victory, or seek a collaborator in a community of problem-solvers. Stay clear of the negativity and “venting” that can come with the Teachers’ Lounge.
          <br><br>
          <span class="note">TeachersConnect requires a modern web browser with cookies and javascript enabled.</span>
        </div>
      <?php } ?>
    </div>

    <div class="login-form">
      <div class="login-form-block">
        <h2>Sign in to TeachersConnect.</h2>
        <form method="post">
          <label for="user">Email Address</label>
          <input type="text" id="user" name="user" value="<?=$email?>">
          <label for="pass">Password</label>
          <input type="password" id="pass" name="pass">
          <input type="submit" value="Sign In">
        </form>
      </div>
      <?php if ($status != 'ready') { ?>
        <div class="login-form-signup">
          <h2>Not a member?  Join today.</h2>
          <a href="<?php Config::PROTOCOL.$_SERVER['SERVER_NAME'];?>/refer.php?ref=auth"><button>Sign Up</button></a>
        </div>
      <?php } ?>
    </div>
