
    <div class="login-callout">
      <img class="login-callout-img-logo-horizontal" src="img/tclogo.svg">
        <img class="login-callout-img-logo-horizontal" src="https://www.teachersconnect.com/wp-content/uploads/2017/06/TC-Online-Mac-Tab-Phone-1024x470.png">
        <div class="login-callout-text permanent">
          <div class="supplement permanent text0">
            <h1>An Uncompromisingly Teacher-centric Online Community of Teachers</h1>
            <br>
            You’re about to enter the most teacher-centric, relentlessly positive online community on the Web. No venting--just teachers solving instructional problems and reaching more students each day. Join, and say goodbye to teaching as a solo activity.
            <br><br>

            <div class="center">
              <!-- <h3>Join tens of thousands of teachers for only $2/month*</h3> -->
              <h3>Join tens of thousands of teachers.</h3>
              <a class="button learn-more" href="https://www.teachersconnect.com/">Learn More</a>
            </div>
            <br><br>
          </div>

        </div>

    </div>

    <div class="login-form">
      <form id="accordion-form" name="registration-form" class="login-form-block form-signup submit-once" method="post">
        <div class="accordion-tab<?php if ($formSubmission == 'fail' AND ((empty(trim($firstName))) OR (empty(trim($lastName))) OR (empty(trim($user))) OR (!filter_var($user, FILTER_VALIDATE_EMAIL)) OR (empty(trim($pass))) OR !$termsAgreement)) echo ' error'; ?>" id="tab-identity">Become a Member Now</div>
        <div class="accordion-panel panel-identity">
          <div class="form-node<?php if ($formSubmission == 'fail' AND (empty(trim($firstName)))) echo ' error'; ?>">
            <label for="firstName">First Name</label>
            <input type="text" id="firstName" name="firstName" value="<?=$firstName?>">
          </div>
          <div class="form-node<?php if ($formSubmission == 'fail' AND (empty(trim($lastName)))) echo ' error'; ?>">
            <label for="lastName">Last Name</label>
            <input type="text" id="lastName" name="lastName" value="<?=$lastName?>">
          </div>
          <div class="form-node<?php if ($formSubmission == 'fail' AND ((empty(trim($user))) OR (!filter_var($_POST['user'], FILTER_VALIDATE_EMAIL)))) echo ' error'; ?>">
            <label for="user">Email Address</label>
            <input type="text" id="user" name="user" value="<?=$user?>">
            <label class="form-check" for="user-check">Confirm Email Address</label>
            <input type="text" id="user-check" name="user-check" value="">
          </div>
          <div class="form-node<?php if ($formSubmission == 'fail' AND (empty(trim($pass)))) echo ' error'; ?>">
            <label for="pass">Password</label>
            <input type="password" id="pass" name="pass" value="<?=$pass?>">
            <label class="form-check" for="pass-check">Confirm Password</label>
            <input type="password" id="pass-check" name="pass-check" value="">
          </div>
          <input type="hidden" id="userType" name="userType" value="other">
          <input type="hidden" id="userRef" name="userRef" value="<?=$refid?>">
          <div class="form-node<?php if ($formSubmission == 'fail' AND !$termsAgreement) echo ' error'; ?>">
            <input type="checkbox" id="termsAgreement" name="termsAgreement" <?php if ($termsAgreement) echo 'checked'; ?>>
            <label for="termsAgreement">I agree to the TeachersConnect <a target="_blank" href="http://www.teachersconnect.com/terms-of-use/">Terms of Use</a> and <a target="_blank" href="http://www.teachersconnect.com/privacy-policy/">Privacy Policy</a>.</label>
          </div>
          <input name="registration-form-submit" type="submit" value="Create">
        </div>
      </form>
      <!-- <div class="footnote">* Multiple membership options are available.</div> -->
    </div>

    <!-- <hr class="clear">
    <div>
      <h1 style="text-align: center;">What is TeachersConnect?</h1>
<p>TeachersConnect is an online community for any teacher and those who prepare, support, and empower them in the classroom. The vibrant community grants teachers access to the most powerful resource of all: each other.</p>
<p>Share ideas and collaborate with other teachers through group discussions and community posts on any device. We believe that every teacher has something to contribute and every teacher who joins makes our teaching community more powerful.</p>
    </div> -->
