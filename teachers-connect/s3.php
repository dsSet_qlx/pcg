<?php
// System Setup
use Aws\S3\S3Client;
use Aws\Exception\AwsException;

if ($_FILES) { // If images supplied, upload to S3

  try {

      //Create a S3Client
      $clientS3 = new S3Client([
        'version'     => Config::AWS_VERSION,
        'region'      => Config::AWS_REGION,
        'endpoint'    => Config::AWS_ENDPOINT,
        'credentials' => [
           'key'      => Config::ACCESS_KEY,
           'secret'   => Config::SECRET_KEY,
        ],
      ]);

      $image_display = [];

      foreach($_FILES as $file) {

        if ($file['error'] == 0) {

          $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
          $size = $file['size'];

          if(in_array($ext,$valid_filetypes) ) {

            $uuid = \Ramsey\Uuid\Uuid::uuid4();
            $file_name = $uuid->toString() . '.' . $ext;

            // putObject method sends data to the chosen bucket
            $response = $clientS3->putObject(array(
                'Bucket'      => Config::BUCKET,
                'Key'         => 'uploads/' . $file_name,
                'SourceFile'  => $file['tmp_name'],
                'ACL'         => 'public-read',
            ));

            $image_display[] = array($file_name, $file['name'], $ext, $size);

          }

        }

      }


  } catch(Exception $e) {
      echo "Error > {$e->getMessage()}";
  }

}
