<?php
// System Setup
require 'includes/startup.php';
// require 'includes/checkup.php';

// if ($_SESSION['user'] == $_SERVER['REMOTE_ADDR']) { // Display view if user has valid session

  // Variable Setup
  $_GET  = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

  if (Config::SERVER != 'maintenance' OR (Config::SERVER == 'maintenance' AND $_SESSION['bsm'])) { // Display site when not in maintenance mode or when bypassing maintenance lock with status

    $file_ext = strtolower(pathinfo(parse_url($_GET['id'])['path'], PATHINFO_EXTENSION));

    // if ( in_array( $file_ext, $image_filetypes ) ) {

      $img = new Imagick(Config::S3URL . $_GET['id']);
      autorotate($img);
      if ($_GET['width']) {
        $img->scaleImage($_GET['width'], 0);
      } elseif ($_GET['height']) {
        $img->scaleImage(0, $_GET['height']);
      }

      header('Pragma: public');
      header('Cache-Control: max-age=86400');
      header('Expires: '. gmdate('D, d M Y H:i:s \G\M\T', time() + 86400));
      header('Content-type: '. $img->getImageMimeType());
      echo $img->getImageBlob();

    // }

    // echo file_get_contents(Config::S3URL . $_GET['id']);

  } else {

    die();

  }

// } else {
//
//   die();
//
// }
